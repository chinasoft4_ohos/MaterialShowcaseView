package uk.co.deanwild.materialshowcaseviewsample;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.shape.OvalShape;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ExampleOhosTest {
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("uk.co.deanwild.materialshowcaseviewsample", actualBundleName);
    }

    @Test
    public void testOvalShape() {
        OvalShape ovalShape = new OvalShape();
        ovalShape.setPadding(10);
        assertNotNull(ovalShape);
    }
}