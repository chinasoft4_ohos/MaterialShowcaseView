/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.deanwild.materialshowcaseviewsample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Text;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.shape.OvalShape;
import uk.co.deanwild.materialshowcaseviewsample.ResourceTable;
import uk.co.deanwild.materialshowcaseviewsample.util.ToastUtil;

/**
 * SimpleSingle界面
 *
 * @since 2021-07-14
 */
public class SimpleSingleExampleAbilitySlice extends AbilitySlice {
    private static final String SHOWCASE_ID = "simple example";
    private static final int CONSTANT_1000 = 1000;
    private Button btnShow;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_simple_single_example);
        Text textTitle = (Text) findComponentById(ResourceTable.Id_text_titleBar_title);
        textTitle.setText("SimpleSingleExample");
        btnShow = (Button) findComponentById(ResourceTable.Id_btn_show);
        Button btnReset = (Button) findComponentById(ResourceTable.Id_btn_reset);

        btnShow.setClickedListener(component -> {
            presentShowcaseView(0);
        });
        btnReset.setClickedListener(component -> {
            MaterialShowcaseView.resetSingleUse(SHOWCASE_ID);
            ToastUtil.getInstance().showToast(getContext(), "Showcase reset");
        });

        // 延迟一秒加载
        presentShowcaseView(CONSTANT_1000);
    }

    private void presentShowcaseView(int withDelay) {
        new MaterialShowcaseView.Builder(getAbility())
                .setTarget(btnShow)
                .setShape(new OvalShape())
                .setTitleText("Hello")
                .setDismissText("GOT IT")
                .setContentText("This is some amazing feature you should know about")
                .setDelay(withDelay)
                .singleUse(SHOWCASE_ID)
                .show();
    }
}
