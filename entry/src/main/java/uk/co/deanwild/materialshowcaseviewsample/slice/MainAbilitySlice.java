/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.deanwild.materialshowcaseviewsample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

import uk.co.deanwild.materialshowcaseviewsample.ResourceTable;
import uk.co.deanwild.materialshowcaseviewsample.SimpleSingleExampleAbility;
import uk.co.deanwild.materialshowcaseviewsample.CustomExampleAbility;
import uk.co.deanwild.materialshowcaseviewsample.SequenceExampleAbility;
import uk.co.deanwild.materialshowcaseviewsample.TooltipExampleAbility;
import uk.co.deanwild.materialshowcaseviewsample.util.ToastUtil;

/**
 * 主页
 *
 * @since 2021-07-14
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private Intent mIntent;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Button btnSimpleExample = (Button) findComponentById(ResourceTable.Id_btn_simple_example);
        Button btnCustomExample = (Button) findComponentById(ResourceTable.Id_btn_custom_example);
        Button btnSequenceExample = (Button) findComponentById(ResourceTable.Id_btn_sequence_example);
        final Button btnTooltipExample = (Button) findComponentById(ResourceTable.Id_btn_tooltip_example);
        final Button btnResetAll = (Button) findComponentById(ResourceTable.Id_btn_reset_all);

        btnSimpleExample.setClickedListener(this);
        btnCustomExample.setClickedListener(this);
        btnSequenceExample.setClickedListener(this);
        btnTooltipExample.setClickedListener(this);
        btnResetAll.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_simple_example:
                startAbilityCustom(SimpleSingleExampleAbility.class);
                break;
            case ResourceTable.Id_btn_custom_example:
                startAbilityCustom(CustomExampleAbility.class);
                break;
            case ResourceTable.Id_btn_sequence_example:
                startAbilityCustom(SequenceExampleAbility.class);
                break;
            case ResourceTable.Id_btn_tooltip_example:
                startAbilityCustom(TooltipExampleAbility.class);
                break;
            case ResourceTable.Id_btn_reset_all:
                MaterialShowcaseView.resetAll();
                ToastUtil.getInstance().showToast(this, "All Showcases reset");
                break;
            default:
                break;
        }
    }

    /**
     * 跳转页面
     *
     * @param cls Ability
     */
    protected void startAbilityCustom(Class<?> cls) {
        if (mIntent == null) {
            mIntent = new Intent();
        }
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withAbilityName(cls)
                .build();
        mIntent.setOperation(operation);

        startAbility(mIntent);
    }
}
