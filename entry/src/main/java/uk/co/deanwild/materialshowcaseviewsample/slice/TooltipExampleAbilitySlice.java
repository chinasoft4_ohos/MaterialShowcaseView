/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.deanwild.materialshowcaseviewsample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Image;
import ohos.agp.components.StackLayout;
import ohos.agp.utils.Color;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;
import uk.co.deanwild.materialshowcaseview.ShowcaseTooltip;
import uk.co.deanwild.materialshowcaseviewsample.ResourceTable;
import uk.co.deanwild.materialshowcaseviewsample.util.ToastUtil;

/**
 * Tooltip界面
 *
 * @since 2021-07-14
 */
public class TooltipExampleAbilitySlice extends AbilitySlice {
    private static final String SHOWCASE_ID = "tooltip example";
    private static final int CONSTANT_15 = 15;
    private static final int CONSTANT_30 = 30;
    private static final int CONSTANT_50 = 50;
    private static final int CONSTANT_500 = 500;
    private StackLayout slTitleBar;
    private Image imageEdit;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_tooltip_example);
        slTitleBar = (StackLayout) findComponentById(ResourceTable.Id_sl_tooltip_example_titleBar);
        Button btnShow = (Button) findComponentById(ResourceTable.Id_btn_show);
        Button btnReset = (Button) findComponentById(ResourceTable.Id_btn_reset);
        imageEdit = (Image) findComponentById(ResourceTable.Id_image_edit);

        btnShow.setClickedListener(component -> {
            presentShowcaseView();
        });
        btnReset.setClickedListener(component -> {
            MaterialShowcaseView.resetSingleUse(SHOWCASE_ID);
            ToastUtil.getInstance().showToast(getContext(), "Showcase reset");
        });

        presentShowcaseView();
    }

    private void presentShowcaseView() {
        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(CONSTANT_500);

        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(getAbility(), SHOWCASE_ID);

        ShowcaseTooltip toolTip1 = ShowcaseTooltip.build(this)
                .corner(CONSTANT_15)
                .arrowIsCenter(true)
                .textColor(Color.getIntColor("#007686"))
                .text(getString(ResourceTable.String_tooltipexampleability_hit_content));

        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(getAbility())
                        .setTarget(slTitleBar)
                        .setToolTip(toolTip1)
                        .withRectangleShape()
                        .setTooltipMargin(CONSTANT_30)
                        .setShapePadding(CONSTANT_50)
                        .setDismissOnTouch(true)
                        .setMaskColour(Color.getIntColor("#CC4f5355"))
                        .build());

        ShowcaseTooltip toolTip2 = ShowcaseTooltip.build(this)
                .corner(CONSTANT_15)
                .textColor(Color.getIntColor("#007686"))
                .text("This is another very funky tooltip");

        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(getAbility())
                        .setTarget(imageEdit)
                        .setToolTip(toolTip2)
                        .setTooltipMargin(CONSTANT_30)
                        .setShapePadding(CONSTANT_50)
                        .setDismissOnTouch(true)
                        .setMaskColour(Color.getIntColor("#CC4f5355"))
                        .build());
        sequence.start();
    }
}
