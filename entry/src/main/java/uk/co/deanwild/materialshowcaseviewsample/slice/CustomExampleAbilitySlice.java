/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.deanwild.materialshowcaseviewsample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Text;
import ohos.agp.components.Image;
import ohos.agp.components.Component;
import ohos.agp.components.AttrHelper;
import ohos.agp.utils.Color;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseviewsample.ResourceTable;
import uk.co.deanwild.materialshowcaseviewsample.util.ToastUtil;

/**
 * Custom界面
 *
 * @since 2021-07-14
 */
public class CustomExampleAbilitySlice extends AbilitySlice {
    private static final String SHOWCASE_ID = "custom example";
    private static final float CONSTANT_40F = 40f;
    private static final int CONSTANT_1000 = 1000;
    private Button btnShow;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_custom_example);
        Text textTitle = (Text) findComponentById(ResourceTable.Id_text_titleBar_title);
        Image imageTitle = (Image) findComponentById(ResourceTable.Id_image_titleBar_right);
        textTitle.setText("CustomExample");
        imageTitle.setVisibility(Component.VISIBLE);
        btnShow = (Button) findComponentById(ResourceTable.Id_btn_show);
        Button btnReset = (Button) findComponentById(ResourceTable.Id_btn_reset);

        imageTitle.setClickedListener(component -> {
            new MaterialShowcaseView.Builder(getAbility())
                    .setTarget(imageTitle)
                    .setShapePadding(AttrHelper.vp2px(CONSTANT_40F,getContext()))
                    .setDismissText("GOT IT")
                    .setContentText(getString(ResourceTable.String_customexampleability_hit_content))
                    .setContentTextColor(new Color(Color.getIntColor("#FF99CC00")))
                    .setMaskColour(Color.getIntColor("#FFAA66CC"))
                    .show();
        });

        btnShow.setClickedListener(component -> {
            presentShowcaseView(0);
        });

        btnReset.setClickedListener(component -> {
            MaterialShowcaseView.resetSingleUse(SHOWCASE_ID);
            ToastUtil.getInstance().showToast(getContext(), "Showcase reset");
        });

        // 延迟一秒加载
        presentShowcaseView(CONSTANT_1000);
    }

    private void presentShowcaseView(int withDelay) {
        new MaterialShowcaseView.Builder(getAbility())
                .setTarget(btnShow)
                .setContentText("This is some amazing feature you should know about")
                .setDismissText("GOT IT")
                .setDismissOnTouch(true)
                .setContentTextColor(new Color(Color.getIntColor("#FF99CC00")))
                .setMaskColour(Color.getIntColor("#FFAA66CC"))
                .setDelay(withDelay)
                .singleUse(SHOWCASE_ID)
                .show();
    }
}
