/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.deanwild.materialshowcaseviewsample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;
import uk.co.deanwild.materialshowcaseviewsample.ResourceTable;
import uk.co.deanwild.materialshowcaseviewsample.util.ToastUtil;

/**
 * Sequence界面
 *
 * @since 2021-07-14
 */
public class SequenceExampleAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private static final String SHOWCASE_ID = "sequence example";
    private static final int CONSTANT_500 = 500;
    private static final String CONSTANT_GOT_IT = "GOT IT";
    private Button btnOne;
    private Button btnTwo;
    private Button btnThree;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_sequence_example);
        Text textTitle = (Text) findComponentById(ResourceTable.Id_text_titleBar_title);
        textTitle.setText("SequenceExample");

        btnOne = (Button) findComponentById(ResourceTable.Id_btn_one);
        btnTwo = (Button) findComponentById(ResourceTable.Id_btn_two);
        btnThree = (Button) findComponentById(ResourceTable.Id_btn_three);
        final Button btnReset = (Button) findComponentById(ResourceTable.Id_btn_reset);

        btnOne.setClickedListener(this);
        btnTwo.setClickedListener(this);
        btnThree.setClickedListener(this);
        btnReset.setClickedListener(this);

        presentShowcaseSequence();
    }

    @Override
    public void onClick(Component component) {
        if (component.getId() == ResourceTable.Id_btn_one
                || component.getId() == ResourceTable.Id_btn_two
                || component.getId() == ResourceTable.Id_btn_three) {
            presentShowcaseSequence();
        } else {
            MaterialShowcaseView.resetSingleUse(SHOWCASE_ID);
            ToastUtil.getInstance().showToast(getContext(), "Showcase reset");
        }
    }

    private void presentShowcaseSequence() {
        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(CONSTANT_500);

        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(getAbility(), SHOWCASE_ID);

        sequence.setOnItemShownListener(new MaterialShowcaseSequence.OnSequenceItemShownListener() {
            @Override
            public void onShow(MaterialShowcaseView itemView, int position) {
                ToastUtil.getInstance().showToast(getContext(), "Item #" + position);
            }
        });

        sequence.setConfig(config);

        sequence.addSequenceItem(btnOne, "This is button one", CONSTANT_GOT_IT);

        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(getAbility())
                        .setTarget(btnTwo)
                        .setSkipText("SKIP")
                        .setDismissText(CONSTANT_GOT_IT)
                        .setContentText("This is button two")
                        .withRectangleShape(true)
                        .build()
        );

        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(getAbility())
                        .setTarget(btnThree)
                        .setDismissText(CONSTANT_GOT_IT)
                        .setContentText("This is button three")
                        .withRectangleShape()
                        .build()
        );

        sequence.start();
    }
}
