/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.deanwild.materialshowcaseviewsample.util;

import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import uk.co.deanwild.materialshowcaseviewsample.ResourceTable;

/**
 * Toast工具类
 *
 * @since 2021-07-14
 */
public class ToastUtil {
    private ToastUtil() {
    }

    /**
     * 实例
     *
     * @return 实例
     */
    public static ToastUtil getInstance() {
        return ToastUtilInstance.INSTANCE;
    }

    /**
     * Toast实例内部类
     *
     * @since 2021-07-14
     */
    private static class ToastUtilInstance {
        private static final ToastUtil INSTANCE = new ToastUtil();
    }

    /**
     * 显示Toast
     *
     * @param context 上下文
     * @param content 内容信息
     */
    public void showToast(Context context, String content) {
        Component toastLayout = LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_layout_toast, null, false);
        Text toastText = (Text) toastLayout.findComponentById(ResourceTable.Id_text_msg_toast);
        toastText.setText(content);
        new ToastDialog(context)
                .setComponent(toastLayout)
                .setTransparent(true)
                .show();
    }
}
