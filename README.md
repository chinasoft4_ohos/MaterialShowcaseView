# MaterialShowcaseView

## 项目介绍

- 项目名称：MaterialShowcaseView
- 所属系列：openharmony的第三方组件适配移植
- 功能：新功能引导高亮显示遮罩层
- 项目移植状态：主功能完成
- 调用差异：调用无差异（因API不支持裁剪任意组件，未实现原库界面扩散动画）
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 1.3.4

## 效果演示

<img src="image/gif.gif"></img>

## 安装教程

1.在项目根目录下的build.gradle文件中添加。
```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```
2.在entry模块的build.gradle文件中

 ```
dependencies {
   implementation('com.gitee.chinasoft_ohos:MaterialShowcaseView:1.0.0')
   ......  
}
 ```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行。如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明

1. 在entry模块下的MyApplication类中初始化数据存储（key、value形式），主要使用全局的上下文，这样数据才能存到全局中。

   ```java
   public class MyApplication extends AbilityPackage {
       @Override
       public void onInitialize() {
           super.onInitialize();
           // 初始化数据存储
           PrefsManager.getInstance().init(this);
       }
   }
   ```

   

2. 在界面中直接调用

```java
public class SimpleSingleExampleAbilitySlice extends AbilitySlice {
    private static final String SHOWCASE_ID = "simple example";
    private static final int CONSTANT_1000 = 1000;
    private Button btnShow;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_simple_single_example);
        btnShow = (Button) findComponentById(ResourceTable.Id_btn_show);
        Button btnReset = (Button) findComponentById(ResourceTable.Id_btn_reset);

        btnShow.setClickedListener(component -> {
            // 直接加载
            presentShowcaseView(0);
        });
        btnReset.setClickedListener(component -> {
            // 重置标记
            MaterialShowcaseView.resetSingleUse(SHOWCASE_ID);
            ToastUtil.getInstance().showToast(getContext(), "Showcase reset");
        });

        // 延迟一秒加载
        presentShowcaseView(CONSTANT_1000);
    }

    private void presentShowcaseView(int withDelay) {
        new MaterialShowcaseView.Builder(getAbility())
                .setTarget(btnShow)
                .setShape(new OvalShape())
                .setTitleText("Hello")
                .setContentText("This is some amazing feature you should know about")
	            .setDismissText("GOT IT")
                .setDelay(withDelay)
                .singleUse(SHOWCASE_ID)
                .show();
    }
}
```

2.方法说明

|      方法      |                             说明                             |
| :------------: | :----------------------------------------------------------: |
|   setTarget    |                     高亮显示的Component                      |
|    setShape    | 高亮显示的形状（圆形 CircleShape、椭圆OvalShape、矩形RectangleShape） |
|  setTitleText  |                       遮罩层提示的标题                       |
| setContentText |                       遮罩层提示的内容                       |
| setDismissText |                       关闭遮罩层的文字                       |
|    setDelay    |                    延迟显示的时间（毫秒）                    |
|   singleUse    |     标记，关闭后下次不再显示（以key、value形式存到本地）     |
|      show      |                             显示                             |


## 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


## 版本迭代

- 1.0.0

## 版权和许可信息

```
Copyright 2015 Dean Wild

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

```