package uk.co.deanwild.materialshowcaseview.shape;


import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import uk.co.deanwild.materialshowcaseview.target.Target;

/**
 * Specifies a shape of the target (e.g circle, rectangle).
 * Implementations of this interface will be responsible to draw the shape
 * at specified center point (x, y).
 */
public interface Shape {

    /**
     * Draw shape on the canvas with the center at (x, y) using Paint object provided.
     *
     * @param canvas canvas
     * @param paint paint
     * @param x x
     * @param y y
     */
    void draw(Canvas canvas, Paint paint, int x, int y);

    /**
     * Get width of the shape.
     *
     * @return int
     */
    int getWidth();

    /**
     * Get height of the shape.
     *
     * @return int
     */
    int getHeight();

    /**
     * Update shape bounds if necessary
     *
     * @param target target
     */
    void updateTarget(Target target);

    int getTotalRadius();

    void setPadding(int padding);
}
