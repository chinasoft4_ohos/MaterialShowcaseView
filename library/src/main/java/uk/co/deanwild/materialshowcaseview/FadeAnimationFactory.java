package uk.co.deanwild.materialshowcaseview;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.utils.Point;


public class FadeAnimationFactory implements IAnimationFactory{

    private static final float INVISIBLE = 0f;
    private static final float VISIBLE = 1f;

    @Override
    public void animateInView(Component target, Point point, long duration, AnimationStartListener listener) {
        AnimatorProperty apAlpha=new AnimatorProperty();
        apAlpha.setStateChangedListener(new StateChangedListenerAdapter() {
            @Override
            public void onStart(Animator animator) {
                super.onStart(animator);
                listener.onAnimationStart();
            }
        });
        apAlpha.setTarget(target)
                .alphaFrom(INVISIBLE).alpha(VISIBLE)
                .setDuration(300)
                .start();
    }

    @Override
    public void animateOutView(Component target, Point point, long duration, AnimationEndListener listener) {
        AnimatorProperty apAlpha=new AnimatorProperty();
        apAlpha.setStateChangedListener(new StateChangedListenerAdapter() {
            @Override
            public void onEnd(Animator animator) {
                super.onEnd(animator);
                listener.onAnimationEnd();
            }
        });
        apAlpha.setTarget(target)
                .alphaFrom(VISIBLE).alpha(INVISIBLE)
                .setDuration(300)
                .start();
    }
}
