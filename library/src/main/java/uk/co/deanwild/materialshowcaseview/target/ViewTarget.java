package uk.co.deanwild.materialshowcaseview.target;


import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;

public class ViewTarget implements Target {

    private final Component mComponent;

    public ViewTarget(Component component) {
        mComponent = component;
    }

    public ViewTarget(int viewId, Ability ability) {
        mComponent = ability.findComponentById(viewId);
    }

    @Override
    public Point getPoint() {
        int x = (int) (mComponent.getContentPositionX() + mComponent.getPivotX());
        int y = (int) (mComponent.getContentPositionY() + mComponent.getPivotY());
        return new Point(x,y);
    }

    @Override
    public Rect getBounds() {
        return new Rect(
                0,
                0,
                mComponent.getEstimatedWidth(),
                mComponent.getEstimatedHeight()
        );
    }

    public Component getView() {
        return mComponent;
    }
}
