package uk.co.deanwild.materialshowcaseview;


import ohos.agp.components.Component;
import ohos.agp.utils.Point;

public interface IAnimationFactory {

    void animateInView(Component target, Point point, long duration, AnimationStartListener listener);

    void animateOutView(Component target, Point point, long duration, AnimationEndListener listener);

    interface AnimationStartListener {
        void onAnimationStart();
    }

    interface AnimationEndListener {
        void onAnimationEnd();
    }
}

