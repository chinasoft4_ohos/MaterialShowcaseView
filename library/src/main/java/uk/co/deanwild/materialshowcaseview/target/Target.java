package uk.co.deanwild.materialshowcaseview.target;


import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;

public interface Target {
    Point getPoint();

    Rect getBounds();
}
