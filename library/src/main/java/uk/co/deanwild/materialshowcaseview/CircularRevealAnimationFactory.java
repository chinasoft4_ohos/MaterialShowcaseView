package uk.co.deanwild.materialshowcaseview;


import ohos.agp.components.Component;
import ohos.agp.utils.Point;

public class CircularRevealAnimationFactory implements IAnimationFactory {

    @Override
    public void animateInView(Component target, Point point, long duration, final AnimationStartListener listener) {

    }

    @Override
    public void animateOutView(Component target, Point point, long duration, final AnimationEndListener listener) {

    }
}
